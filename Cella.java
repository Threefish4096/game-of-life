import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

class Cella extends Rectangle {
    private static final int DIM_CELLA = 10;

    private int riga;
    private int colonna;
    private Persona persona;

    public Cella(int riga, int colonna) {
        this.riga = riga;
        this.colonna = colonna;
        this.persona = null;
        initializeCell(Color.WHITE);
    }

    public Cella(Persona persona, Color colore, int riga, int colonna) {
        this.riga = riga;
        this.colonna = colonna;
        this.persona = persona;
        initializeCell(colore);
    }

    private void initializeCell(Color colore) {
        this.setFill(colore);
        this.setWidth(DIM_CELLA);
        this.setHeight(DIM_CELLA);
    }

    public int getRiga() {
        return riga;
    }

    public int getColonna() {
        return colonna;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
