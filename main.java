import java.util.LinkedList;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.CacheHint;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {
    private static final int NUM_ROWS = 60;
    private static final int NUM_COLUMNS = 80;
    private static final int NUM_INITIAL_PERSONS = (NUM_ROWS * NUM_COLUMNS) / 2;

    private static Cella[][] arena = new Cella[NUM_ROWS][NUM_COLUMNS];
    private static int numPersonsTotal = 0;
    private static LinkedList<Persona> membersList = new LinkedList<>();
    private static GridPane grid;
    private static Random random = new Random();

    public void start(Stage primaryStage) {
        initializeGrid();

        for (int i = 0; i < NUM_INITIAL_PERSONS; i++) {
            createRandomPerson();
        }
        numPersonsTotal = NUM_INITIAL_PERSONS;

        initializeNeighbors();

        startThreads();

        primaryStage.setTitle("Game of Life");
        primaryStage.setScene(new Scene(grid, NUM_COLUMNS * 13, NUM_ROWS * 10));
        primaryStage.setResizable(false);

        optimizeGrid();

        new AnimationTimer() {
            public void handle(long now) {
                updateUI();
            }
        }.start();

        primaryStage.show();
    }

    private void initializeGrid() {
        grid = new GridPane();

        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLUMNS; j++) {
                arena[i][j] = new Cella(i, j);
            }
        }
    }

    private void createRandomPerson() {
        int ageLowerBound = 18;
        int ageUpperBound = 60;
        int ageRange = ageUpperBound - ageLowerBound + 1;

        Persona person = new Persona(random.nextBoolean(), 5, random.nextInt(ageRange) + ageLowerBound);

        int row = random.nextInt(NUM_ROWS);
        int column = random.nextInt(NUM_COLUMNS);

        while (arena[row][column].getPersona() != null) {
            row = random.nextInt(NUM_ROWS);
            column = random.nextInt(NUM_COLUMNS);
        }

        arena[row][column].setPersona(person);
        membersList.add(person);

        person.setRow(row);
        person.setColumn(column);

        if (person.getAge() != 0 && person.getAge() > 18) {
            arena[row][column].setFill(Color.BLACK);
            grid.add(arena[row][column], column, row, 1, 1);
        } else {
            arena[row][column].setFill(Color.GREEN);
            grid.add(arena[row][column], column, row, 1, 1);
        }
    }

    private void initializeNeighbors() {
        for (Persona member : membersList) {
            member.PingVicini(arena);
        }
    }

    private void startThreads() {
        for (Persona member : membersList) {
            member.start();
        }
    }

    private void optimizeGrid() {
        grid.setCache(true);
        grid.setCacheHint(CacheHint.SPEED);
        grid.setCacheShape(true);
        grid.setStyle("-fx-background-color:#FFFFFF; -fx-opacity:10;");
    }

    private void updateUI() {
        for (int i = 0; i < membersList.size(); i++) {
            Persona person = membersList.get(i);
            int row = person.getRow();
            int column = person.getColumn();

            if (arena[row][column].getPersona() != null) {
                if (!person.isInterrupted()) {
                    if (person.getAge() < 18) {
                        arena[row][column].setFill(Color.GREEN);
                        Cella cell = new Cella(person, Color.GREEN, row, column);
                        grid.add(cell, column, row, 1, 1);
                    } else {
                        if (person.getAge() == 120 || person.getContaMorte() == 10) {
                            arena[row][column].setFill(Color.WHITE);
                            Cella cell = new Cella(person, Color.WHITE, row, column);
                            grid.add(cell, column, row, 1, 1);
                            membersList.remove(person);
                            arena[row][column].setPersona(null);
                        }
                    }
                }
            } else {
                arena[row][column].setFill(Color.WHITE);
                Cella cell = new Cella(null, Color.WHITE, row, column);
                grid.add(cell, column, row, 1, 1);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
