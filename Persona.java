import java.util.LinkedList;

class Persona extends Thread {
    private int row, column; // Position in the matrix
    private int age; // Age required for reproduction [18-50], beyond which not possible
    private int happiness; // Indicator [0-10]
    private int deathCount; // Counter cycles that stay at 0, if it reaches 3, the person dies
    private int lifeCount; // Counter that counts for how long the person is happy, if it reaches 3, they have a child

    private boolean gender; // true = male, false = female

    private Persona mother = null; // Reference to the person's mother
    private Persona father = null; // Reference to the person's father

    private LinkedList<Persona> contacts = new LinkedList<>(); // Neighbors [0-8]

    Persona(boolean gender, int happiness, int age) {
        this.gender = gender;
        this.happiness = happiness;
        this.age = age;
        this.deathCount = 0;
        this.lifeCount = 0;
    }

    Persona(Persona father, Persona mother) {
        this.contacts = new LinkedList<>();
        this.father = father.gender ? father : mother;
        this.mother = father.gender ? mother : father;
        happiness = 7;
        this.gender = Main.random.nextBoolean();
        this.deathCount = 0;
        this.lifeCount = 0;
        this.age = 0;
    }

    void send(Persona recipient) {
        if (recipient != null) {
            try {
                modifyParameters(Main.random.nextInt(10), recipient);
            } catch (InterruptedException e) {
                System.err.print("Error in wait()");
            }
        }
    }

    void wait(int i) throws InterruptedException {
        Thread.sleep(i);
    }

    void modifyParameters(int parameter, Persona recipient) throws InterruptedException {
        switch (parameter) {
            case 0:
                modifyParameters(recipient, -2);
                break;
            case 1:
            case 2:
            case 3:
                modifyParameters(recipient, -1);
                break;
            case 6:
            case 7:
            case 8:
            case 9:
                modifyParameters(recipient, 1);
                break;
            case 10:
                modifyParameters(recipient, 2);
                break;
        }
    }

    synchronized void modifyParameters(Persona recipient, int param) throws InterruptedException {
        wait(150);
        recipient.happiness = Math.max(0, Math.min(10, recipient.happiness + param));
    }

    public void run() {
        while (!this.interrupted()) {
            contacts.forEach(this::send);

            if (contacts.size() < 3 || happiness == 0 || contacts.size() > 5) {
                deathCount++;
            }

            if (happiness >= 7) {
                lifeCount++;
                deathCount = 0;
            }

            if (lifeCount == 2 && age > 18 && age <= 60) {
                reproduce(this);
                lifeCount = 0;
            }

            contacts.removeIf(contact -> contact.isInterrupted());

            if (age == 120 || deathCount == 10) {
                synchronized (this) {
                    Main.numTotalPeople--;
                }
                synchronized (this) {
                    Main.arena[row][column].persona = null;
                }
                interrupt();
                break;
            }
            age++;
        }
    }

    void reproduce(Persona person) {
        if (person.contacts.size() == 8) {
            System.err.println("I have no space around me!");
            return;
        }

        LinkedList<Persona> possibleSpouses = new LinkedList<>();

        for (Persona contact : person.contacts) {
            if (person.gender != contact.gender && contact.age > 18 && contact.age <= 60) {
                possibleSpouses.add(contact);
            }
        }

        if (!possibleSpouses.isEmpty()) {
            Persona spouse = possibleSpouses.get(Main.random.nextInt(possibleSpouses.size()));
            haveChild(this, spouse);
        }
    }

    void haveChild(Persona person, Persona spouse) {
        Persona child = new Persona(person, spouse);
        int newRow, newColumn;

        do {
            newRow = person.mother.row + Main.random.nextInt(3) - 1;
            newColumn = person.mother.column + Main.random.nextInt(3) - 1;
        } while (Main.arena[newRow][newColumn].persona != null);

        createChild(child, newRow, newColumn);
    }

    void createChild(Persona child, int row, int column) {
        synchronized (this) {
            Main.arena[row][column].persona = child;
            Main.numTotalPeople++;
        }
        child.row = row;
        child.column = column;
        synchronized (this) {
            Main.arena[row][column].persona.start();
        }
    }

    public void pingNeighbors(Cella arena[][]) {
        for (int i = 0; i < arena.length; i++) {
            for (int j = 0; j < arena[0].length; j++) {
                addContact(arena, i, j);
            }
        }
    }

    void addContact(Cella arena[][], int row, int column) {
        if (arena[row][column].persona != null && contacts.size() < 8) {
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (i != 0 || j != 0) {
                        int newRow = row + i;
                        int newColumn = column + j;

                        if (isValidPosition(newRow, newColumn) && !contacts.contains(arena[newRow][newColumn].persona)) {
                            contacts.add(arena[newRow][newColumn].persona);
                        }
                    }
                }
            }
        }
    }

    private boolean isValidPosition(int row, int column) {
        return row >= 0 && row < Main.rows && column >= 0 && column < Main.columns;
    }
}
